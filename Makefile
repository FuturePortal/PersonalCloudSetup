# ===========================
# Default: help section
# ===========================

info: intro
intro:
	@echo ""
	@echo "██████╗ ██╗ ██████╗██╗  ██╗   ███╗   ██╗██╗   ██╗     ██████╗██╗      ██████╗ ██╗   ██╗██████╗"
	@echo "██╔══██╗██║██╔════╝██║ ██╔╝   ████╗  ██║██║   ██║    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗"
	@echo "██████╔╝██║██║     █████╔╝    ██╔██╗ ██║██║   ██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║"
	@echo "██╔══██╗██║██║     ██╔═██╗    ██║╚██╗██║██║   ██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║"
	@echo "██║  ██║██║╚██████╗██║  ██╗██╗██║ ╚████║╚██████╔╝    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝"
	@echo "╚═╝  ╚═╝╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝      ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝"

# ===========================
# Main commands
# ===========================

update: \
	intro \
	do-setup-helm \
	do-install-ingress-nginx \
	do-install-cert-manager \
	do-cert-manager-acme-dns \
	do-apply-kubernetes-manifests

# ===========================
# Common recipes
# ===========================

do-setup-helm:
	@echo ""
	@echo "=== Setting up helm requirements ==="
	@echo ""
	helm repo add jetstack https://charts.jetstack.io
	helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
	helm repo update

do-install-ingress-nginx:
	@echo ""
	@echo "=== Installing nginx ingress ==="
	@echo ""
	helm upgrade --wait --install ingress-nginx ingress-nginx/ingress-nginx -f config/nginx-ingress.yml

do-install-cert-manager:
	@echo ""
	@echo "=== Installing cert-manager ==="
	@echo ""
	helm upgrade --wait --install cert-manager jetstack/cert-manager --namespace cert-manager -f config/cert-manager.yml --set crds.enabled=true

do-cert-manager-acme-dns:
	@echo ""
	@echo "=== Set ACME DNS01 resolver ==="
	@echo ""
	echo $$ACME_DNS > acme-dns.json
	kubectl delete secret -n cert-manager acme-dns || echo "ACME-DNS secret doesn't exist yet"
	kubectl create secret -n cert-manager generic acme-dns --from-file acme-dns.json

do-apply-kubernetes-manifests:
	@echo ""
	@echo "=== Apply kubernetes manifests ==="
	@echo ""
	kubectl apply -f kubernetes/
