# How to Kubernetes on Digital Ocean

In this project you deploy the services that are needed for the cluster to operate like it should.
in [my opensource website](https://gitlab.com/FuturePortal/RickvanderStaaij/tree/production/kubernetes)
you can see how to setup the kubernetes configurations for a specific website deployment.

# Project specific files

When setting up a project, take a look at [my opensource website](https://gitlab.com/FuturePortal/RickvanderStaaij/tree/production/kubernetes),
here you can find how to correctly setup everything for your project. Combine that with this project, and you can run your project on your own cluster!

## .gitlab-ci.yml

Copy the setup in this file (and in my opensource website), make sure you connect to the right registry.
In this case we use registry.gitlab.com

Also copy the `kubernetes/` directory.

## Create docker pull secret

In order for your Kubernetes cluster to access your registry, a
secret needs to be generated.

In gitlab, create a new access token. Make sure the access token has
access to the registry. Copy the key you receive to the docker to the
command below.

Run the following command in the cluster terminal:

```sh
kubectl create secret docker-registry gitlab-com --docker-email=<email-from-gitlab> --docker-password=<access-token-key> --docker-server=<registry-url> --docker-username=<username>
```

### Automate in GitLab

```.gitlab-ci.yml
.create-registry-pull-secret: &create-registry-pull-secret |
  kubectl create secret docker-registry gitlab-registry \
    --namespace $NAMESPACE \
    --docker-server=$CI_REGISTRY \
    --docker-username=$REGISTRY_READ_USERNAME \
    --docker-password=$REGISTRY_READ_PASSWORD \
  || true

deploy application:
  stage: deploy
  script:
    # Prepare the kubernetes files with environment variables
    - export NAMESPACE=$PRODUCTION_NAMESPACE
    - *create-registry-pull-secret
```

## Wildcard certificates

Check https://cert-manager.io/docs/configuration/acme/dns01/acme-dns/ on how to make a wildcard certificate
